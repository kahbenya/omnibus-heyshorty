#
# Copyright 2016 Yuri Lee
#
# All Rights Reserved.
#

name "heyshorty"
maintainer "Yuri Lee"
homepage "https://gitlab.com/kahbenya"

install_dir "#{default_root}/#{name}"

build_version Omnibus::BuildVersion.semver
build_iteration 1

replace "heyshorty"
conflict "heyshorty"

override :ruby, version: '2.3.1', source: { md5: '0d896c2e7fd54f722b399f407e48a4c6' }
override :rubygems, version: '2.6.6'
override :nginx, version: "1.9.1", source: { md5: "fc054d51effa7c80a2e143bc4e2ae6a7" }
#override :ncurses ,version: "6.0-20150810" , source: { md5: "78bfcb4634a87b4cda390956586f8f1f" } #, url: "ftp://invisible-island.net/ncurses/current/ncurses-6.0-20150810.tgz"
#version("6.0-20150810") { source md5: "78bfcb4634a87b4cda390956586f8f1f", url: "ftp://invisible-island.net/ncurses/current/ncurses-6.0-20150810.tgz" }

#version: "1.9.1", source: { md5: "fc054d51effa7c80a2e143bc4e2ae6a7" }


# Creates required build directories
dependency "preparation"
#dependency "package-scripts"

# heyshorty dependencies/components
# dependency "somedep"

# Version manifest file
#dependency "version-manifest"

#runtime_dependency "libsqlite3-0"

dependency "nginx"
dependency "unicorn"
dependency "nginx-config"
dependency "unicorn-config"
#dependency "jemalloc"
dependency "heyshorty-app"


exclude "**/.git"
exclude "**/bundler/git"
