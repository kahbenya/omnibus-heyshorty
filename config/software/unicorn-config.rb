

#
# Copyright 2016 Yuri Lee
#
# All Rights Reserved.
#

name "unicorn-config"
maintainer "Yuri Lee"


license "MIT"

source path: File.expand_path("config/templates/unicorn/", Omnibus::Config.project_root)


build do
	copy "heyshorty_unicorn.service", "#{install_dir}/embedded/scripts/heyshorty_unicorn.service"
end
