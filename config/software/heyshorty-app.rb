#
# Copyright 2016 Yuri Lee
#
# All Rights Reserved.
#

name "heyshorty-app"
maintainer "Yuri Lee"

license "MIT"

dependency "ruby"
dependency "bundler"
dependency "libxml2"
dependency "libxslt"
dependency "sqlite3"

source git: 'https://gitlab.com/kahbenya/heyshorty'

build do
	env = with_standard_compiler_flags(with_embedded_path)
	Omnibus.logger.add(Omnibus::Logger::LEVELS.index('INFO'),"heyshorty-env") do 
		"Current env #{env}" 
	end

	command "mkdir -p #{install_dir}/embedded/gems"
	#bundle  "install --path=#{install_dir}/embedded/gems --jobs 5 --without development test --gemfile=#{install_dir}/embedded/heyshorty/Gemfile", :env => env
	bundle  "install --path=#{install_dir}/embedded/gems --jobs 5 --without development test", :env => env

	command "mkdir -p #{install_dir}/embedded/heyshorty"
	sync "./" , "#{install_dir}/embedded/heyshorty" , exclude: ['*.sock','*.pid*','*.log','*.un*','*.sw*']

end
