
#
# Copyright 2016 Yuri Lee
#
# All Rights Reserved.
#

name "nginx-config"
maintainer "Yuri Lee"


license "MIT"

source path: File.expand_path("config/templates/nginx/", Omnibus::Config.project_root)

build do
	command "mkdir -p #{install_dir}/embedded/sites-available"
	command "mkdir -p #{install_dir}/embedded/scripts/"

	copy "sites-available/heyshorty" , "#{install_dir}/embedded/sites-available/heyshorty"
	copy "config/nginx.conf", "#{install_dir}/embedded/conf/nginx.conf"
	copy "heyshorty_nginx.service", "#{install_dir}/embedded/scripts/heyshorty_nginx.service"
end
